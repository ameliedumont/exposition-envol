---
title: Home
---

# Un regard sur les migrations

## Introduction

L'exposition «&nbsp;Un regard sur les migrations&nbsp;», bien qu'elle soit truffée de nombreux éléments historiques, ne prétend pas au but encyclopédique d'être un recueil exhaustif de l'histoire des migrations internationales. L’intention de notre exposition, qui se présente tel un récit, est ancrée dans un objectif d'éducation permanente&nbsp;: nous souhaitons documenter et contextualiser un sujet dont les complexités peuvent parfois sembler insaisissables et qui laisse malheureusement trop souvent place au caractère réducteur des préjugés.

Cette exposition invite d'abord le public, par le biais d’un lexique, à remettre en question ses connaissances des mots de la migration, parfois utilisés de manière erronée ou méconnue. Nous proposons ensuite de démarrer ce récit des migrations humaines dès la Préhistoire. Au fil des panneaux explicatifs, une frise historique et multi-chronologique permet de se replonger au cœur d'une histoire vieille comme le monde, à la manière d'un regard jeté sur les migrations au fil du temps. Le récit se poursuit en s'affinant, se précisant de plus en plus dans une perspective centrée sur la Belgique et l'histoire de ses migrations, entre émigrants et immigrants, au fil des siècles. Ce voyage est jalonné d'événements tragiques, que l'exposition ne désire pas occulter, car la migration n'est pas toujours désirée par celles et ceux qui sont amenés à être déplacés à travers le monde. L'exposition se propose également d'aborder des sujets douloureux tels que la colonisation ou encore des déplacements de populations dans le cadre de génocides ou de guerres. 

«&nbsp;Un regard sur les migrations&nbsp;» permet en outre au public de s'interroger sur la notion de frontières, de la manière dont celles-ci sont non seulement conçues, perçues ou pensées mais également de l'impact qu'elles peuvent avoir sur la vie de ceux qui décident de les franchir.

La présente exposition a été conçue en 2019 et 2020, exposée pour la première fois en décembre 2020 au PointCulture Bruxelles, et n’a pas été mise à jour depuis. De nouveaux événements sont bien sûr venus s’ajouter depuis lors à l’histoire migratoire contemporaine. L’exposition est aujourd’hui accessible en format numérique et téléchargeable librement sur notre site internet. L'ASBL L'Envol des frontières a conçu cette exposition dans un esprit de tolérance et de respect et nous souhaitons qu’elle soit utilisée à ces fins. Nous espérons qu'elle permettra à tout un chacun d'appréhender les migrations humaines dans leur globalité et de se détacher des discours de méfiance ou incitant au rejet de l'autre. 

### Un mot sur le traitement du genre dans notre exposition
      
Les «&nbsp;migrants&nbsp;» ne sont pas un groupe homogène. Les migrations touchent à la fois les hommes, les femmes, les personnes transgenres ou encore les enfants, peu importe leur âge, leur sexe, leur nationalité ou leur orientation sexuelle. Notre exposition se veut inclusive&nbsp;: lorsque nous y parlons des «&nbsp;migrants&nbsp;», nous entendons par là toute personne qui se déplace dans le monde, à l'intérieur ou à l'extérieur de son pays ou de sa région d'origine, vers un nouveau lieu de vie.
