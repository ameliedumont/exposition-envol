---
title: Sources et crédits
layout: credits
---

## Sources

- Amnesty International, « Dossier pédagogique 2016 : La migration ici et ailleurs ». 2016
- Caestecker F., « Histoire de la migration en Belgique aux XIX<sup>e</sup> et XX<sup>e</sup> siècles (1830-2004) ». Penser l'immigration et l'intégration autrement. Une initiative belge inter-universitaire. Bruylant. 2006. ISBN 2-8027-2099-6
- Catella C., Aiello S., « Un paese di Calabria ». <br/>Tita Productions. 2017
- CGRA, www.cgra.be , consulté en 2019
- CIRÉ asbl, « Frontex, une agence dangereuse ? », www.cire.be , consulté en mai 2020
- CIRÉ asbl, « La crise à la frontière gréco-turque expliquée », www.cire.be , consulté en mai 2020
- CIRÉ asbl, « Les émigrants belges d'hier, un miroir pour aujourd'hui... : Cahier pédagogique ». 2014
- CIRÉ asbl, « Les émigrants belges d'hier, un miroir pour aujourd'hui... : Brochure ». 2014
- CIRÉ asbl, « Lexique. Réfugié, demandeur d'asile, migrant... Lexique et définitions. ». Juin 2019. P.3-6
- CIRÉ asbl, « Régularisation : où en est-on ? ». Novembre 2014
- Département de la population des Nations Unies, cité par Catherine Wihtol de Wenden, « Les migration au XXI<sup>e</sup> siècle ». Les Experts du Dessous des cartes – ARTE
- DISCRI, « Fiche contenu informatif & explicatif : Historique des immigrations depuis l'après-guerre ».  Guide de délivrance du programme d'intégration citoyenne aux personnes primo-arrivantes (Document évolutif). Août 2014
- Godin M., Rea A., « La campagne de régularisation de 2000 en Belgique : une analyse genrée ». Centre d'information et d'études sur les migrations internationales. Migrations Société. 2010/3 n°129-130. P.75-90.
- L'Illustré, « Jean Ziegler : " Nous avons recréé des camps de concentration " », www.illustre.ch , consulté en mai 2020
- Le Vif, « Nos solutions pour résoudre la crise migratoire ». Numéro 20. 16 mai 2019
- Lire et écrire en Wallonie, Storme A., Mottin S., Godenir A., « Les primo-arrivants : qui sont-ils et quelle place ont-ils dans les politiques d'alphabétisation ? ». Novembre 2012
- Martiniello M., Rea A., « Une brève histoire de l'immigration en Belgique ». Fédération Wallonie-Bruxelles. Bruxelles. 2013. ISBN 978-2-9601251-1-5
- Martiniello M., Rea A., Timmerman C., Wets J., « Nouvelles migrations et nouveaux migrants en Belgique ». Société et Avenir. Politique scientifique fédérale. Academia Press. Gand. 2010. ISBN 978-90-382-1533-4, p. 167
- Morelli A., « Les émigrants belges ». Éditions Couleur Livres. Bruxelles. 1998
- Myria, « Citoyens du Monde. L'histoire de nos migrations. Dossier pédagogique. ». Myriapolis. Décembre 2015. P.4-22
- Myria, « Crise de l'asile de 2015 : des chiffres et des faits ». 2015
- Myria, « Focus : Afflux de demandeurs d'asile et politique d'accueil en Belgique : Comparaison des années 2000 et 2015 ». 2015
- Organisation Internationale pour les Migrations, « L'OIM signale un millier de décès dans la Méditerranée », www.iom.int/fr , consulté en mai 2020
- Organisation Internationale pour les Migrations, « Termes clés de la migration », www.iom.int/fr , consulté en 2019
- RTBF, « Jean Ziegler : " Nous, citoyennes et citoyens, détenons le pouvoir de la honte " », www.rtbf.be , consulté en mai 2020
- Tihon M., « Refugees trapped between Turkey and Greece ». Hans Lucas. Février 2020

## Crédits

### Commissariat d'exposition

ASBL L'Envol des frontières

L'ASBL l'Envol des frontières, créée en décembre 2017, a pour objectif de sensibiliser et d'informer les citoyens et citoyennes sur la thématique des migrations humaines, par le biais d'expositions et d'événements culturels. Indignés par le traitement médiatique et politique de la « crise des réfugiés » des années 2015, 2016 et 2017, les fondateurs de l'ASBL ont souhaité, au travers de l'exposition « Un regard sur les migrations », recontextualiser les migrations contemporaines et les débats y afférents au sein de l'histoire de l'humanité, en les considérant de manière holistique.

La position de notre ASBL est d'informer sur les migrations humaines et de mettre en lumière leurs complexités, leur intersectionnalité, leurs variétés. Avant tout, nous souhaitons rappeler que les migrations sont un phénomène humain qui a toujours existé, et qui va continuer d'exister. Dans un monde en mouvement, ces dernières seront en effet constitutives du XXI<sup>e</sup> siècle, notamment dues aux phénomènes climatiques qui pousseront bientôt des millions de personnes sur les routes de l'exil.

### Design graphique

[Amélie Dumont](https://www.amelie.tools/)

