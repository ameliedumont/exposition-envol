---
title: Aux origines
layout: panneau
weight: 2
---

## Aux origines

Vous le savez sans doute : nous sommes tous des descendants de nomades. En fait, depuis son apparition en tant qu'espèce, l'être humain a majoritairement mené une vie errante.

Les hominidés sont originaires du continent africain d'où ils essaimeront rapidement. Des chercheurs ont pu découvrir des traces de nos ancêtres en Afrique orientale datant de 6 à 7 millions d'années. Les proto-hommes, des espèces apparentées à l'Homo sapiens et désormais disparues, ne se risqueront au voyage vers l'Europe qu'il y a à peine 700.000 à 800.000 ans. L'Homo sapiens, lui, ne serait arrivé en Europe occidentale que 10.000 à 40.000 ans avant notre ère.

L'Europe contemporaine est le résultat d'un grand melting-pot remontant à l'ère glaciaire et les européens sont le résultat d'un mélange de vieilles lignées originaires d'Afrique, du Moyen-Orient et des steppes de Russie.

Au cours de leur vie nomade, les chasseurs-cueilleurs se déplacent en suivant les mouvements des grands troupeaux, ce qui leur permettra de se répandre notamment sur le continent européen.

À l'aide de méthodes agricoles toujours plus efficaces et à la domestication des animaux, l'Homo sapiens commence à se fixer et à mener une vie sédentaire.

Les premières civilisations sédentaires sont sans doute apparues en Égypte, en Mésopotamie et dans la vallée de l'Indus, dans l'actuel Pakistan. Devenu agriculteur, l'Homo sapiens entre dans le Néolithique et connaît un boom démographique. 

{{< cartes >}}

![](/images/carte01.svg)

{{< /cartes >}}