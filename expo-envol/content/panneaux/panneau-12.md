---
title: L'immigration en Belgique - 4
layout: panneau
weight: 13
---

## L'immigration en Belgique - 4

En 1965, les autorités belges cherchent à augmenter l'immigration familiale en remboursant la moitié du prix du voyage des femmes et des enfants. Une immigration clandestine acceptée se développe, avant que la conjoncture ne change et que le chômage ne reparte à la hausse. Dès lors, les autorités s'emploient à respecter à nouveau strictement la législation.

Mais ces mesures auront peu d'impact car déjà en 1968, 62% des travailleurs d'origines étrangères sont issus de la Communauté Européenne. La Communauté Européenne va ainsi conduire à une distinction entre les étrangers issus de la Communauté, protégés par le droit européen, et ceux qui ne le sont pas.

En 1969, le Ministre de l'Emploi et du Travail mettra en place trois mesures destinées à diminuer drastiquement le nombre de permis de travail accordés. En premier lieu, de nouveaux permis ne seront plus délivrés et il n'y aura plus de régularisation à la suite d'un visa touristique. En second lieu, l'accès à un emploi dans un autre secteur que celui pour lequel le permis de travail a été délivré est interdit. En troisième lieu, les personnes d'origines étrangères étant au chômage devront être expulsées. Ces mesures provoquent alors la colère des organisations syndicales qui parviendront à faire retirer ces mesures en vertu de l'égalité des travailleurs belges et immigrés.

Comme pour les communautés italiennes, on voit petit à petit naître des quartiers tantôt espagnols, tantôt portugais ou grecs mais également marocains ou turcs. On prend également conscience à cette époque du rôle positif de l'immigration sur la démographie. Si de 1948 à 1966, l'immigration est très majoritairement masculine, le rapport des sexes se trouve de plus en plus équilibré entre 1967 à 1993.

Depuis 1991, le regroupement familial, les demandes d'asile et l'immigration irrégulière de travail sont les trois principales raisons d'immigration.

Au 1<sup>er</sup> janvier 2010, quasiment 1 personne sur 10 en Belgique était de nationalité étrangère. Parmi ces 10%, 7 sont originaires de l'intérieur de l'Union Européenne pour 3 issus d'un pays tiers. Après l'Europe, l'Afrique est le deuxième continent en nombre de ressortissants à immigrer en Belgique.

{{< cartes >}}

![](/images/image12_1.svg)
![](/images/image12_2.svg)
![](/images/image12_3.svg)
![](/images/image12_4.svg)

{{< /cartes >}}