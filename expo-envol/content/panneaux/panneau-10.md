---
title: L'immigration en Belgique - 2
layout: panneau
weight: 11
---

## L'immigration en Belgique - 2

Durant l'entre-deux guerres, l'hostilité se fait sentir à l'égard des immigrants tandis que leurs possibilités pour obtenir la nationalité belge sont de plus en plus limitées et que les « étrangers de 2<sup>e</sup> génération » doivent prouver qu'ils étaient de « bons patri-otes » pour obtenir la nationalité.

La citoyenneté et le suffrage universel vont encore marquer d'avantage la distinction entre les Belges et « les autres ». Cela n'empêchera pas la Belgique d'accueillir les réfugiés juifs fuyant l'Allemagne nazie dans les années 30. Toutefois, ces derniers ne sont pas reconnus en tant que réfugiés mais uniquement tolérés.

À la fin de la Seconde Guerre mondiale, la production des charbonnages belges a été réduite de moitié, entravant l'économie et la reconstruction du pays. La différence sera comblée temporairement par la mise au travail de 46.000 prisonniers de guerre et ce jusqu'en 1947. Primordiaux pour la Belgique mais boudés par les travailleurs nationaux en raison de la pénibilité et de la dangerosité du travail, les charbonnages se mettent alors en quête d'une main d'œuvre flexible et à bas prix. L'Europe s'étant scindée en deux blocs, les ressortissants des pays de l'Est ne sont plus une option et c'est vers l'Italie que les campagnes de recrutement vont se diriger, à la manière de ce qu'elles étaient auparavant.

En 1946, la Belgique et l'Italie signent un accord destiné à envoyer 50.000 travailleurs italiens contre la promesse de vendre 200 kg de charbon par jour et par mineur à l'Italie. Entre 1946 et 1948, ce sont 85 convois qui amènent les travailleurs italiens aux cinq bassins charbonniers mais la Belgique n'a pas prévu de dispositif d'accueil, surtout concernant les logements. Ainsi, jusqu'en 1950, ces travailleurs seront logés dans d'anciens camps de prisonniers construits par les Allemands, participant à la grande désillusion qui les frappe. Cette solution de logement présentée comme provisoire va en fait se maintenir : en 1956, ce sont 3.389 familles qui occupent toujours 1.939 baraquements en ruines.

Surveillés durant leur transport jusqu'en Belgique, ceux qui ne peuvent supporter les conditions abominables des mines sont considérés comme en rupture de contrat et renvoyés en Italie…

{{< cartes >}}

![](/images/carte10.svg)

{{< /cartes >}}
