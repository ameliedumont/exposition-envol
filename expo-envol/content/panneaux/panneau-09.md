---
title: L'immigration en Belgique - 1
layout: panneau
weight: 10
---

## L'immigration en Belgique - 1

Alors que la Belgique, à l'instar d'autres pays européens était majoritairement un pays d'émigration, la situation s'inverse à la sortie de la Première Guerre mondiale. Dès 1918, toutes les personnes étrangères sont contrôlées à la frontière et se voient demander un passeport et un visa belge afin d'entrer sur le territoire, entravant ainsi la libre circulation des personnes.

Le gouvernement belge, désireux de faire tourner la prospère industrie du charbon, entame des campagnes de recrutement à destination de travailleurs étrangers. Les secteurs de l'industrie lourde, des mines, du textile, de la métallurgie, de la confection et du bâtiment sont également concernés. Entre 1920 et 1930, ce sont 170.000 personnes qui émigrent vers la Belgique dans le but de répondre à l'appel de main d'œuvre. La plupart sont originaires d'Italie ou de l'Europe de l'Est. Concrètement, c'est le besoin de l'industrie qui va dicter la politique migratoire belge.

Néanmoins, la crise de 1929 et le chômage qu'elle occasionne vont engendrer une réduction radicale de l'immigration. Les ouvriers ne sont pourtant pas les seuls à émigrer en Belgique, il y a également des entrepreneurs désireux de lancer ou faire croître leur activité. Avec ce contexte de crise, l'installation de ces nouveaux arrivants va créer de vives réactions xénophobes à leur encontre car ils sont perçus comme des concurrents.

En 1930, le gouvernement belge établit comme condition pour les étrangers désirant travailler en Belgique d'obtenir un contrat de travail et l'autorisation préalable du Ministre de la Justice. De plus, en 1933, la nationalité devient un critère pour l'affiliation aux caisses de chômage, le but étant de faire en sorte que les per-sonnes étrangères sans emploi rentrent au pays. Le parcours se complexifie donc pour ceux qui désirent venir travailler en Belgique car ils doivent désormais bénéficier d'une double autorisation, celle du contrat de travail et celle du visa.

Le Gouvernement conservera cependant une voie d'exception pour les personnes désirant travailler dans des secteurs boudés par les travailleurs nationaux, à l'image de celui des mines.

{{< cartes >}}

![](/images/carte09.svg)

{{< /cartes >}}