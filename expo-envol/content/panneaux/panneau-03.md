---
title: Colonisations et esclavagisme
layout: panneau
weight: 4
---

## Colonisations et esclavagisme

Dès le XV<sup>e</sup> siècle et durant le XVI<sup>e</sup> siècle, des explorateurs se chargent de repousser les frontières du monde connu des Européens à l'aide de leurs navires. La « découverte » de nouvelles régions du monde sera à l'origine de nouveaux déplacements massifs de populations.

Du XVI<sup>e</sup> siècle au XIX<sup>e</sup> siècle, des millions d'Européens vont entreprendre de s'installer dans les colonies que les différentes puissances ont accaparées et annexées. Par exemple, le Portugal, l'Espagne ou encore l'Angleterre se bâtiront un empire colonial, forts des nombreux territoires qu'ils ont annexés. On estime qu'entre 1820 et 1914, ce sont 60 millions d'Européens qui décident de partir s'installer dans les Amériques. Si certains s'y rendent par opportunité, d'autres s'y rendent pour échapper aux persécutions religieuses.

De l'autre côté du miroir, les résidents initiaux des Amériques se voient imposer la présence des Européens qui conquièrent leurs terres, souvent par la force et dans la violence. De plus, les germes amenés par les Européens seront dévastateurs pour les populations locales et petit à petit, par les massacres et les maladies, le continent se dépeuple.

En parallèle des migrations volontaires que constitue la venue des colons en Amérique, des migrations forcées se mettent en place dans le but de faire venir de la main d'œuvre sur place. À cette fin, des marchands achetaient des esclaves contre des tissus, de l'alcool et des armes sur les côtes de l'Afrique occidentale pour ensuite traverser l'Atlantique et y revendre les esclaves contre des matières premières introuvables en Europe. Ce commerce triangulaire, aussi appelé traite transatlantique, et l'esclavage sur lequel il repose conduiront plus de 15 millions d'Africains à être réduits en esclavage et transportés dans des conditions inhumaines jusqu'en Amérique pendant près de 400 ans. L'esclavage ne sera aboli aux États-Unis qu'en 1865 mais ses souvenirs pèsent encore sur les épaules de millions de descendants d'esclaves aujourd'hui.

{{< cartes >}}

![](/images/carte03_1.svg)
![](/images/carte03_2.svg)
![](/images/carte03_3.svg)

{{< /cartes >}}