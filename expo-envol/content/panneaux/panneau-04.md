---
title: Jusqu'à nos jours
layout: panneau
weight: 5
---

## Jusqu'à nos jours

Bien sûr, la colonisation ne concerne pas uniquement le continent américain et, partout, les Européens s'accapareront de nouvelles terres et d'innombrables richesses, au grand dam des populations locales.

Au XX<sup>e</sup> siècle, ce sont notamment les guerres qui entraîneront d'importants déplacements de populations. De nombreuses personnes fuiront les régimes autoritaires qui prennent le pouvoir dans leur pays comme le fascisme en Italie, le régime nazi en Allemagne ou encore la dictature de Franco en Espagne.

Les déportations de millions de Juifs, dont 6 millions seront assassinés par le régime nazi, et la déportation de 3 millions de personnes par l'Union soviétique sont les macabres résultats des déplacements forcés que connaissent alors certains Européens.

Si les deux guerres mondiales ont également pour conséquence d'organiser une immigration économique afin de reconstruire les pays détruits, on constate également des mouvements de retour d'exilés qui peuvent enfin revenir dans leur patrie.

La décolonisation et l'indépendance que conquièrent les anciens peuples colonisés dans les années 1960 et 1970 vont également participer à d'importants mouvements de populations.

Aujourd'hui, en Europe, les personnes à immigrer sont plus nombreuses que les Européens émigrants. Ces Européens émigrent notamment beaucoup vers l'intérieur d'un autre pays de l'Union Européenne.

{{< cartes >}}

![](/images/carte04_1.svg)
![](/images/carte04_2.svg)

{{< /cartes >}}