---
title: L'immigration en Belgique - 3
layout: panneau
weight: 12
---

## L'immigration en Belgique - 3

Les travailleurs italiens sont parfois accompagnés de leur famille et petit à petit la question de la scolarisation et de l'apprentissage de la langue se pose. Si le travail et l'évolution dans la classe ouvrière sert d'intégration aux parents, l'école jouera ce rôle pour certains des enfants. Mais les difficultés que ceux-ci rencontreront à l'école ne leur permettront pas toujours de s'épanouir comme ils le devraient.

Les accidents et les conditions de travail abominables dans les mines conduisent l'Italie à sus-pendre à de multiples reprises l'envoi de ses travailleurs en Belgique. La catastrophe du Bois-du-Cazier en 1956, dont les victimes étaient principalement italiennes, aura pour conséquence de stopper l'émigration de travailleurs italiens, désormais interdite par l'Italie.

Plutôt que d'améliorer les conditions dans les mines, la Belgique va alors se remettre en quête de main d'œuvre étrangère ailleurs qu'en Italie. Durant les Golden Sixties et avec le déclin progressif de l'industrie des mines, les emplois et les destinations de la main d'œuvre d'origine étrangère se diversifient. La Belgique signera de nouvelles conventions bilatérales pour la venue de travailleurs avec l'Espagne (1956), la Grèce (1957), le Maroc (1964), la Turquie (1964), la Tunisie (1969), l'Algérie (1970) et la Yougoslavie (1970).

Entre 1961 et 1970, la population d'origine étrangère augmentera de 263.000 personnes, notamment du fait du regroupement familial. Les femmes trouveront également un emploi, le plus souvent dans les secteurs du nettoyage, de la domesticité ou de l'aide aux personnes.

La demande de main d'œuvre est si forte que les règles préalables comme l'obtention du permis de travail avant celle du permis de séjour ne sont plus forcément respectées.

{{< cartes >}}

![](/images/carte11.svg)

{{< /cartes >}}
