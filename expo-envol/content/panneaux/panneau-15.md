---
title: La gestion des frontières de l'Union
layout: panneau
weight: 16
---

## La gestion des frontières de l'Union

Nous avons vu que les différents États membres confient la gestion des frontières extérieures de l'Union à cette dernière et par là-même à son agence FRONTEX. Par l'intermédiaire de cette agence aux budgets colossaux, l'Union mène une politique mortifère aux abords de ses frontières car l'intensification des contrôles rend les migrants toujours plus dépendants de réseaux criminels organisant leur traversée par des chemins aux multiples obstacles toujours plus périlleux. Trop souvent, certains y perdent la vie. Rien qu'entre 2014 et 2018, l'Organisation internationale pour les migrations a recensé au moins 1.000 morts par an rien que dans la Méditerranée. Plutôt que d'organiser des voies de migration sécurisées et légales, la politique de l'Union transforme la Méditerranée en un cimetière nimbé de camps. 

Tandis que les embarcations transportant les migrants au travers de la Méditerranée sont interceptées et reconduites sur les côtes d'Afrique du Nord, des « hotspots » s'érigent en effet aux pourtours du bassin méditerranéen. Les hotspots, ce sont des camps comme ceux que l'on peut trouver sur les îles grecques de Lesbos, Samos, Chios et Leros. Normalement destinés à accueillir les demandeurs en attente de leur enregistrement et de la possibilité d'introduire une demande d'asile au sein d'un des états de l'Union, ces camps participent à une stratégie de dissuasion et de terreur au service d'une Europe qui se veut forteresse.

Dans le camp de Moria sur l'île de Lesbos, où un important incendie s'est déclaré en septembre 2020, plus de 20.000 personnes vivaient dans cet espace initialement prévu pour 2.500. Nombre d'entre eux sont mineurs. Les conditions d'hygiène y sont déplorables voire inexistantes, la nourriture avariée, le camp est infesté par les rats et les serpents et tous n'ont pour toit que des tentes de fortune ou des petits containers brûlants dans lesquels s'entassent plusieurs familles. Les suicides, tout comme les injustices, se multiplient au sein de ces camps.

{{< cartes >}}

![](/images/carte16.svg)

{{< /cartes >}}