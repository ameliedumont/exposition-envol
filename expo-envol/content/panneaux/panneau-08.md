---
title: Les émigrants belges - 3
layout: panneau
weight: 9
---

## Les émigrants belges - 3

Le XX<sup>e</sup> siècle a également connu son lot de migrations massives, dues notamment aux guerres mondiales, au boom économique des années 60, au conflit en ex-Yougoslavie ou encore au fossé croissant entre le Sud et le Nord du monde.

Les entreprises belges s'étaient implantées en Russie avant la Première Guerre mondiale, amenant avec elles des ingénieurs mais également des milliers d'ouvriers spécialisés. En 1910, ils étaient 22.500 Belges à s'être installés en Russie, attirés par l'idée de pouvoir bénéficier de meilleures conditions de vie et salariales en vue d'épargner et d'envoyer de l'argent à leur famille. Ils y gagnent une certaine réussite sociale. Même au sein de l'usine, il semble cependant qu'une distance demeure entre les Belges et les Russes. On remarque qu'ils « refusent d'apprendre la langue » ou encore qu'ils conservent leur religion, faisant même venir de loin un prêtre catholique pour baptiser leurs enfants.

Durant la Première Guerre mondiale, au moment de l'invasion allemande en 1914, entre 1.300.000  et 1.500.000 Belges fuient pour se réfugier dans des pays voisins, terrorisés par les récits des exactions de l'armée allemande. En quelques semaines seulement, un cinquième de la population belge quittera le pays. Désormais réfugiés, ils seront plus d'un million aux Pays-Bas, 200.000 en Angleterre et 250.000 en France.

Les Belges sont bien accueillis dans ces pays voisins, qui imaginent que la guerre sera brève et que leur séjour prendra fin avec celle-ci, mais au fil du temps, les relations se dégra-dent, surtout en Angleterre, ce qui conduira notamment à une méfiance redoublée lors de l'arrivée de réfugiés belges au cours de la Seconde Guerre mondiale. On leur octroie alors une allocation pour survivre mais ce sont principalement des associations bénévoles qui s'occuperont des réfugiés belges.

L'invasion allemande au début de la Seconde Guerre mondiale provoquera un exode précipité de plus de 2 millions de Belges sur les routes.

En 2017, on comptait 37.557 Belges ayant quitté le pays pour 24.100 y revenant.

{{< cartes >}}

![](/images/carte08.svg)

{{< /cartes >}}
