---
title: Les émigrants belges - 2
layout: panneau
weight: 8
---

## Les émigrants belges - 2

Les pays frontaliers de la Belgique ne sont évidemment pas les seules destinations choisies par les Belges ayant décidé d'émigrer. À partir du XIX<sup>e</sup> siècle, l'invention du bateau à vapeur permet de faciliter le voyage vers des horizons plus lointains et notamment les Amériques.

En Belgique, les récoltes de pommes de terre des années 1846 et 1847 sont exceptionnellement mauvaises et à partir des années 1870, une crise du secteur agricole touche le pays, n'atteignant son apogée qu'entre 1885 et 1895. Ceci poussera de nombreux paysans et ouvriers agricoles belges à émigrer en quête de terres et d'une nouvelle vie.

De nombreuses brochures incitent les Belges à émigrer aux États-Unis, vu comme un paradis sur terre. On y vante la possibilité d'acheter des terrains à bas prix, ce qui représente une opportunité et attire les agriculteurs alors en difficulté. Ceux qui décident d'entreprendre le voyage vendent leurs biens en Belgique et embarquent pour une traversée de l'Atlantique dans des conditions très dures, vers un pays dont ils ne connaissent pas la langue. Les terres qu'ils ont achetées sont en réalité de véritables forêts vierges et le climat est loin d'être aussi doux que ce qui leur avait été promis.

De 1850 à 1856, les autorités belges vont envoyer aux Amériques des mendiants, des vagabonds et des détenus libérés. Ces traversées se font dans des conditions extrêmes et parfois, les difficultés se poursuivent après la traversée comme lorsque des indigents de Grand-Leez durent traverser le Lac Michigan gelé à pied pendant près de 300 km. Néanmoins, les Belges ayant émigré se disent heureux d'être partis dans les lettres qu'ils envoient en Belgique, ce qui déclenchera une ruée d'émigration en direction du Wisconsin, aux États-Unis.

D'autres brochures encouragent également les Belges à émigrer vers le Canada dès le milieu du XIX<sup>e</sup> siècle. Celles-ci enjolivent également la réalité tout en atténuant les craintes des Belges au sujet du climat. Ces brochures insistent sur la possibilité de cultiver de grandes étendues de terre et de se développer tout en promulguant des salaires avantageux pour les ouvriers. Ces derniers seront sans doute les plus déçus. Au total, 32.000 Belges émigreront au Canada entre 1901 et les années 30. Arrêtée à la suite de la crise économique des années 30, cette immigration des Belges au Canada ne reprendra qu'à partir des années 50.

{{< cartes >}}

![](/images/carte07.svg)

{{< /cartes >}}