---
title: Lexique des mots de la migration
layout: panneau
weight: 1
---

## Lexique des mots de la migration

{{< definitions >}}

<details open>
<summary>Migration interne</summary>

Ce type de migration sert à décrire le déplacement des individus qui décident de s'installer dans une autre partie du pays où ils sont nés.

</details>

<details open>
<summary>Migration internationale</summary>

La migration internationale rend compte du déplacement d'une personne qui vit dans un autre pays que celui où elle est née depuis une durée supérieure ou égale à 1 an.
</details>

<details open>
<summary>Migration forcée</summary>

Celle-ci concerne les déplacements non-volontaires de populations chassées de leurs lieux de vie en raison de divers motifs. Soit pour fuir un conflit armé ou une situation de violence, soit par crainte de persécutions en raison de leur appartenance politique, religieuse ou ethnique ou pour fuir ces persécutions, soit en raison d'une déportation, soit parce qu'ils sont contraints de se déplacer en raison d'une catastrophe naturelle ou d'un désastre industriel.

</details>

<details open>
<summary>Migration économique</summary>

Ce type de migration concerne les individus quittant leur pays d'origine pour s'installer sur le territoire d'un État étranger afin d'améliorer leurs conditions d'existence. On les distingue généralement des réfugiés qui, eux, fuient des persécutions en raison de leur appartenance politique, religieuse ou ethnique.

</details>

<details open>
<summary>Migration de travail</summary>

Les migrants sont alors munis d'un titre de séjour pour s'installer sur le territoire d'un État étranger, conforme au droit applicable, et séjournent dans le pays d'emploi pour une période délimitée, spécifiée dans le contrat de travail.

</details>

<details open>
<summary>Migration étudiante</summary>

Celle-ci concerne les personnes se déplaçant de manière durable dans un autre pays afin d'y poursuivre des études. Ceci peut concerner un programme d'études à l'étranger, un Erasmus ou encore un master complémentaire.

</details>

<details open>
<summary>Migration climatique</summary>

Cette catégorie, plus récente, rend compte du fait que les causes climatiques des migrations vont tendre à augmenter et à se diversifier dans le futur, obligeant de nombreux individus à trouver un nouveau lieu de vie. Les effets du réchauffement climatiques sont variés&nbsp;: sécheresses, inondations, malnutrition, destruction des cultures, maladies… Les personnes amenées à se déplacer dans ce cadre peuvent le faire au sein de leur État de résidence ou en direction d'un pays étranger.

</details>

<details open>
<summary>Migrant</summary>

Un migrant s'entend de toute personne qui, quittant son lieu de résidence habituelle, franchit une frontière ou se déplace à l'intérieur d'un État quels que soient&nbsp;:

- Le statut juridique de la personne
- Le caractère, volontaire ou involontaire, du déplacement
- Les causes du déplacement
- La durée du séjour

</details>

<details open>
<summary>Demandeur d'asile</summary>

Un demandeur d'asile est une personne qui est arrivée dans un pays et y a introduit une demande d'asile auprès des autorités compétentes, qui devront déterminer si elle a effectivement besoin d'une protection internationale. En cas de réponse positive, la personne reçoit le statut de réfugié.e et le droit de rester dans le pays.

</details>

<details open>
<summary>Réfugié</summary>

Un réfugié a traversé une frontière, fuyant son pays car il craint d'y être persécuté pour des raisons politiques, religieuses ou communautaires, et trouve refuge dans un autre pays. Le statut de « réfugié » est internationalement reconnu et confère un accès à l'assistance des États, des Nations Unies et d'autres organisations.

</details>

<details open>
<summary>Primo-arrivant</summary>

Un demandeur d'asile dont la demande a été acceptée et qui a acquis le statut de réfugié sera également considéré comme primo-arrivant en Belgique.

</details>

<details open>
<summary>Personne déplacée interne</summary>

Une personne déplacée interne (PDI) recherche la sécurité dans une autre partie de son propre pays. Elle se déplace soit pour les mêmes raisons qu'un réfugié, soit à la suite d'une catastrophe naturelle ou climatique.

</details>

<details open>
<summary>Asile</summary>

Toute personne qui a fui son pays parce que sa vie ou son intégrité y étaient menacées, et qui craint d'y retourner, peut demander une protection à la Belgique. C'est ce qu'on appelle «&nbsp;demander l'asile&nbsp;». La loi belge prévoit deux statuts de protection&nbsp;: le statut de réfugié et la protection subsidiaire.

</details>

<details open>
<summary>Centre fermé</summary>

Lieu de privation de liberté où sont détenues des personnes en séjour irrégulier en attente d'être expulsées et, dans certains cas de figure, des demandeurs d'asile. Officiellement, un centre fermé n'est pas une prison, même si les similitudes avec le système pénitentiaire sont flagrantes. Il y a actuellement 5 centres fermés en Belgique, dont l'un est prévu pour enfermer des familles avec enfants. Environ 600 personnes peuvent être détenues dans ces centres. Il est prévu que trois nouveaux centres soient construits d'ici 2022. 

</details>

<details open>
<summary>Commissariat Général aux Réfugiés et aux Apatrides (CGRA)</summary>

Instance centrale de la procédure d'asile en Belgique. Administration indépendante, le CGRA a pour mission d'examiner les demandes d'asile et d'accorder ou de refuser le statut de réfugié ou la protection subsidiaire.

</details>

<details open>
<summary>Convention de Genève</summary>

Convention internationale (Nations Unies, 1951) relative au statut des réfugiés. Elle définit ce qu'est un réfugié, ainsi que les droits et les devoirs de ces personnes. La Belgique, comme tous les États membres de l'Union européenne, a signé cette convention.

</details>

<details open>
<summary>Demandeur «&nbsp;de seconde zone&nbsp;»</summary>

Se dit de demandeurs d'asile n'ayant pas accès à la procédure d'asile ordinaire, et ayant une procédure plus rapide et moins de garanties procédurales, notamment un délai d'introduction de recours plus court.

</details>

<details open>
<summary>Détention administrative</summary>

La détention administrative est une mesure prise par l'Office des étrangers qui entraîne l'enfermement de personnes étrangères dans l'attente, soit d'une autorisation de séjour en Belgique, soit d'une expulsion. La détention n'est pas considérée comme une sanction, mais comme un moyen d'exécuter une mesure d'expulsion.

</details>

<details open>
<summary>Droit à la vie privée et familiale</summary>

Droit fondamental garanti par plusieurs conventions internationales et européennes. Il inclut par exemple le fait de pouvoir vivre avec sa famille, d'avoir une vie sociale et des relations avec la personne de son choix, de voir ses données personnelles protégées…

</details>

<details open>
<summary>Fedasil</summary>

Agence fédérale pour l'accueil des demandeurs d'asile. Fedasil organise l'accueil des demandeurs d'asile en Belgique&nbsp;: elle gère notamment des centres d'accueil et coordonne des programmes de retour volontaire.


</details>

<details open>
<summary>Français langue étrangère (FLE)</summary>

Langue française enseignée à des non francophones. On le distingue du français langue seconde (pour des personnes qui ont déjà eu contact avec le français comme deuxième langue). En Belgique, l'usage est de parler de cours de «&nbsp;FLE&nbsp;» pour les personnes scolarisées et cours d'«&nbsp;alpha-FLE&nbsp;» pour les personnes non alphabétisées.

</details>

<details open>
<summary>Garanties de retour</summary>

Preuves qu'un étranger doit fournir pour certifier qu'il va rentrer dans son pays d'origine à la fin de son visa de court séjour (moins de 3 mois). Sur cette base, la Belgique évalue le risque d'immigration irrégulière&nbsp;: l'Office des étrangers examine par exemple la situation familiale, professionnelle et socio-économique de la personne dans son pays d'origine.

</details>

<details open>
<summary>Haut-Commissariat pour les réfugiés (HCR)</summary>

L'agence des Nations Unies qui dirige et coordonne l'action internationale pour protéger les personnes déracinées dans le monde&nbsp;: demandeurs d'asile, réfugiés, personnes déplacées, apatrides…

</details>

<details open>
<summary>Inadmissible / « INAD »</summary>

La Belgique désigne comme « inadmissible » (ou « INAD ») un ressortissant étranger arrivé sur le territoire en provenance d'un pays tiers auquel on refuse l'accès au territoire, le plus souvent à l'aéroport de Zaventem.

</details>

<details open>
<summary>Maison de retour</summary>

Maison, appartement ou studio où l'Office des étrangers peut placer des familles avec enfants mineurs ayant fait l'objet d'un refus d'accès au territoire ou d'un ordre de quitter le territoire parce qu'elles n'avaient pas de droit de séjour. On en compte 27 en Belgique, pour une capacité totale de 169 lits. Même si les familles ne sont pas enfermées ni gardées, elles sont assignées à y résider et considérées comme détenues d'un point de vue juridique.

</details>

<details open>
<summary>Migrants en transit</summary>

Ceux qui sont regroupés sous le terme «&nbsp;migrants en transit&nbsp;» sont des personnes entrées sur le territoire de l'Union européenne le plus souvent via le sud de l'Europe. Elles souhaitent se rendre dans un autre pays européen (généralement le Royaume-Uni), qui n'est en principe pas responsable en vertu du Règlement Dublin, pour y demander l'asile ou y séjourner (pour des raisons familiales ou liées au travail ou aux études, par exemple).

</details>

<details open>
<summary>Office des étrangers (OE)</summary>

Administration qui assiste le Ministre de l'Intérieur et le secrétaire d'État à l'Asile et la Migration dans la gestion de la politique des étrangers en Belgique. Elle traite notamment les demandes de séjour (regroupement familial, régularisation…), enregistre les demandes d'asile, gère les centres fermés et organise les retours forcés.

</details>

<details open>
<summary>Ordre de quitter le territoire (OQT)</summary>

Décision notifiée par l'Office des étrangers et par laquelle l'État ordonne à une personne étrangère de quitter le territoire belge dans un délai déterminé.

</details>

<details open>
<summary>Parcours d'accueil et d'intégration</summary>

Accompagnement des personnes primo-arrivantes dans l'apprentissage de la langue, de la citoyenneté et de l'insertion socio-professionnelle, avec l'objectif qu'elles puissent mener une vie autonome et participer à la société belge. Ce parcours est obligatoire dans les trois régions, même si l'obligation n'est pas encore effective à Bruxelles.

</details>

<details open>
<summary>Personne vulnérable</summary>

Selon la loi belge sur l'accueil des demandeurs d'asile, il s'agit notamment des mineurs, des parents isolés accompagnés d'enfants mineurs, des femmes enceintes, des personnes âgées, des personnes ayant un handicap ou gravement malades, des personnes qui ont subi des tortures, des viols ou d'autres formes graves de violence psychologique, physique ou sexuelle.

</details>

<details open>
<summary>Protection internationale</summary>

Ce terme renvoie en Belgique aux deux statuts de protection qui peuvent être accordés aux personnes qui ont fui leur pays d'origine et qui ne peuvent être protégées par les autorités de leur propre pays&nbsp;: le statut de réfugié et le statut de protection subsidiaire.

</details>

<details open>
<summary>Protection subsidiaire</summary>

Statut octroyé aux personnes qui n'obtiennent pas le statut de réfugié mais à l'égard desquelles il y a de sérieux motifs de croire que, si elles étaient renvoyées dans leur pays d'origine, elles encourraient un risque réel de subir des atteintes graves (comme la peine de mort, la torture ou la violence aveugle dans le cadre d'un conflit armé).

</details>

<details open>
<summary>Règlement Dublin</summary>

Réglementation européenne qui s'applique aux États membres de l'Union européenne ainsi qu'à la Suisse, la Norvège, l'Islande, le Liechtenstein. Elle prévoit qu'un seul État est responsable de traiter une demande d'asile. Sur cette base, le premier pays d'entrée irrégulière en Europe est le plus souvent considéré comme responsable.

</details>

<details open>
<summary>Regroupement familial</summary>

Procédure qui permet aux membres de famille étrangers de personnes vivant légalement en Belgique d'obtenir un visa ou un titre de séjour. Pour faire venir sa famille grâce au regroupement familial, une personne qui réside en Belgique doit remplir des conditions de revenus, de logement et de mutuelle.

</details>

<details open>
<summary>Régularisation</summary>

Procédure permettant à une personne sans-papiers de demander une autorisation de séjour en Belgique pour des raisons humanitaires (article 9bis) ou médicales (article 9ter).

</details>

<details open>
<summary>Retour forcé</summary>

Aussi appelé éloignement ou expulsion. Mise en œuvre par les autorités, sous la contrainte, d'une décision notifiée à un étranger de quitter le territoire parce qu'il n'y a pas ou plus de droit de séjour (rapatriement) ou parce que l'accès au territoire lui a été refusé (refoulement). Le droit international interdit de renvoyer une personne vers un pays où elle risque de subir des persécutions ou des violations graves de ses droits fondamentaux, en vertu du principe de «&nbsp;non-refoulement&nbsp;».

</details>

<details open>
<summary>Retour volontaire</summary>

Retour d'une personne dans son pays d'origine ou dans un pays tiers sur le territoire duquel elle est admise à séjourner, suite à une décision autonome de faire appel à un programme d'assistance au retour mis en place par les autorités du pays d'accueil.

</details>

<details open>
<summary>Sans-papiers, personne en séjour irrégulier</summary>

Retour d'une personne dans son pays d'origine ou dans un pays tiers sur le territoire duquel elle est admise à séjourner, suite à une décision autonome de faire appel à un programme d'assistance au retour mis en place par les autorités du pays d'accueil.

</details>

<details open>
<summary>Séjour étudiant</summary>

Pour résider en Belgique sous le statut d'étudiant, la personne étrangère doit poursuivre des études dans l'enseignement supérieur ou être inscrite dans une année préparatoire à l'enseignement supérieur. Certaines conditions doivent être remplies concernant les études, les moyens financiers, la santé et la sécurité.

</details>

<details open>
<summary>Visa</summary>

Document officiel délivré par les autorités compétentes d'un pays qu'un étranger doit présenter lors de son entrée sur le territoire de celui-ci. Il s'agit d'une condition nécessaire pour entrer et séjourner sur le territoire mais pas d'une garantie. En effet, les autorités aux frontières de certains pays peuvent refuser l'admission sur leur territoire de tout étranger, même si celui-ci détient un visa valide.

</details>

<details open>
<summary>Visa humanitaire</summary>

Visa donné par un pays via ses postes diplomatiques à l'étranger. Il peut être de court séjour (moins de 3 mois) ou de long séjour (plus de 3 mois). C'est le secrétaire d'État à l'Asile et à la Migration et l'Office des Étrangers qui décident de les délivrer.

</details>

{{< /definitions >}}
