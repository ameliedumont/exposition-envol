---
title: Et en Belgique ?
layout: panneau
weight: 6
---

## Et en Belgique ?

Avant de proclamer son indépendance en 1830, la Belgique aura été l'objet de nombreux changements en fonction des puissances qui occupent son territoire. La Belgique aura entre autres fait partie de l'Empire Romain, de l'Empire de Charlemagne et été sous la domination espagnole, des Habsbourg d'Autriche, de la France et des Pays-Bas. Toutes ces annexions amènent leur flot de représentants de la nouvelle autorité en place.

Déjà avant son indépendance, la Belgique sert de terre d'accueil pour les voyageurs et commerçants mais aussi pour les réfugiés. Si la plupart des personnes qui immigrent en Belgique proviennent de pays proches, elles fuient parfois également un conflit comme les Juifs fuyant les pogroms dont ils sont victimes en Russie, les civils lors de la guerre franco-allemande ou encore les survivants de la Commune de Paris.

Notre région accueille également des ouvriers hautement qualifiés. En fait, toute personne capable de s'entretenir seule est la bienvenue, sans besoin d'aucun document car les mouvements des individus ne sont alors pas considérés comme un problème. La société ne fait à l'époque pas de différence entre les Belges et les personnes étrangères qui s'installent dans le pays. Ils sont néanmoins soumis aux mêmes obligations que les Belges et acquièrent la qualité de résident à la suite d'un court séjour.

Le concept de nationalité ne prend forme que durant le XIX<sup>e</sup> siècle. C'est en effet à partir de la fin des années 1880, à la suite de l'accroissement progressif du rôle régulateur de l'État dans les matières sociales, qu'une distinction va s'établir entre d'une part les Belges et de l'autre les « étrangers ». Quant aux émig-rants qualifiés de « non-résidents », la politique d'immigration sera de plus en plus répressive à leur égard dans le but d'exclure les étrangers sans emploi.

La création des frontières internationales va ralentir les migrations internationales tandis que l'industrialisation mène à l'exode rural, c'est-à-dire à des déplacements de populations intérieurs au pays. Au XIX<sup>e</sup> siècle, la Belgique était avant tout un pays d'émigration, le nombre de personnes en situation de migration arrivant en Belgique étant inférieur au nombre de personnes la quittant.

{{< cartes >}}

![](/images/carte05.svg)
![](/images/carte05_2.svg)

{{< /cartes >}}