---
title: Les émigrants belges - 1
layout: panneau
weight: 7
---

## Les émigrants belges - 1

En Belgique, on a souvent l'habitude d'entendre parler d'immigration comme si, en dehors de leurs vacances, les belges, eux, n'émigraient pas. C'est pourtant faux, pour hier comme pour aujourd'hui.

Déjà au XVI<sup>e</sup> siècle, et ce afin de fuir la reconquête espagnole menée par Alexandre Farnèse, on assiste à une émigration massive d'artisans, de marchands et d'intellectuels de Flandre et du Brabant en direction de la Hollande. Ils emmènent avec eux leurs capitaux et leurs savoir-faire et participent ainsi au rayonnement de la Hollande. En raison de la tenue vestimentaire des brabançons, les poètes et pasteurs locaux mettent en garde la population hollandaise contre ces manifestations extérieures de différences. On dit aussi des belges qu' « ils restent entre eux » tandis que 43% des hommes originaires d'Anvers alors installés à Amsterdam épouseront une Anversoise entre 1585 et 1592.

Jusque peu avant la Première Guerre mondiale, on constate que le nombre de Belges à émigrer était plus important que le nombre de personnes étrangères à s'installer en Belgique. Si certains partent s'installer très loin de la Belgique, beaucoup choisissent d'émigrer plus près de chez eux.

Au XIX<sup>e</sup> siècle, c'est en France que les Belges sont les plus nombreux à émigrer, notamment dans le Nord. À partir de 1850, les Belges représentent même la plus grosse communauté étrangère. Tandis que beaucoup iront travailler dans l'industrie du textile ou du charbon dans le Nord, nombre de femmes belges seront employées à Paris comme domestiques. Même s'ils sont appréciés pour la qualité de leur travail et le fait qu'ils acceptent un salaire inférieur à celui de leurs camarades français, ils font également face à de nombreux préjugés. Les réactions anti-belges qui accompagnent les difficultés économiques et la hausse du chômage touchent tous les Belges vivant en France mais surtout les néerlandophones. En 1901, 323.390 Belges vivaient en France dont près de 200.000 uniquement dans le département du Nord.

{{< cartes >}}

![](/images/carte06.svg)

{{< /cartes >}}