---
title: Et l'Union Européenne ?
layout: panneau
weight: 15
---

## Et l'Union Européenne ?

Avant 1992, les différents États membres de l'Union Européenne, selon le principe de la souveraineté nationale, étaient seuls responsables des conditions et de la décision quant à l'acceptation de personnes étrangères. À partir de 1992, les États membres acceptent de réglementer les questions liées à l'asile et l'immigration dans un cadre commun au niveau européen. Dès lors, ils ne pourront plus contrôler les personnes se déplaçant entre différents pays à l'intérieur de l'Union ni gérer individuellement les flux migratoires en direction de l'Union. C'est ainsi qu'un régime commun, le RAEC pour Régime d'Asile Européen Commun, voit le jour. Il a alors pour but d'offrir aux demandeurs d'asile et aux bénéficiaires d'une protection, un statut et une protection uniforme sur l'ensemble du territoire de l'Union. À l'heure actuelle, c'est encore cependant loin d'être le cas.

Les accords de Schengen, entrés en vigueur en 1995, et signés par tous les membres de l'UE à l'exception du Royaume-Uni et de l'Irlande, visent à garantir et organiser la sécurité à l'intérieur de cet espace. Entre autres mesures, on compte : la sup-pression des contrôles aux frontières intérieures donnant lieu à un contrôle renforcé auprès des frontières extérieures, l'harmonisation des conditions d'accès à l'espace pour de courts séjours, la définition des règles relatives à l'asile avec notamment le règlement Dublin II, le système commun Eurodac et la création du système d'information Schengen (SIS).

Le règlement de Dublin est une législation européenne établissant les critères et mécanismes qui servent à déterminer quel État membre sera responsable de l'examen d'une demande de protection internationale introduite par un individu ressortissant d'un pays tiers ou un apatride dans l'un des États membres. Un État qui serait alors requis de prendre en charge le demandeur ne peut s'y soustraire qu'en prouvant qu'il n'est pas responsable selon le règlement de Dublin. Ainsi, le règlement fixe la responsabilité d'un seul État pour une demande et vise à éviter les mouvements secondaires entre les pays, ou le prétendu « shopping de l'asile ».

Néanmoins, à l'heure actuelle, ce règlement est injuste et inefficace, comme le relèvent Amnesty International et de nombreuses autres associations et acteurs de l'asile, car dans les faits, les différents États ne respectent pas de la même manière les droits des demandeurs d'asile. Ceci engendre une véritable loterie de l'asile ne prenant pas en compte les préférences des demandeurs et scellant leur sort en fonction de la désignation de l'un ou l'autre État comme responsable. Malheureusement, le règlement de Dublin reste à l'heure actuelle la pierre angulaire du RAEC.

{{< cartes >}}

![](/images/carte15.svg)

{{< /cartes >}}