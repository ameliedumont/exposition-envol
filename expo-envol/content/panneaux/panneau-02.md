---
title: De la sédentarisation au Moyen-Âge
layout: panneau
weight: 3
---

## De la sédentarisation au Moyen-Âge

Si l'agriculture et la domestication des animaux ont permis la sédentarisation, les déplacements humains n'ont jamais cessé, le plus souvent à petite échelle. Néanmoins, les déplacements s'accélèrent à l'aide de moyens de locomotion plus rapides, plus sûrs et plus confortables.

Au fil des siècles, l'Homo sapiens s'installe sur les pourtours de la Méditerranée, sur un territoire qui correspond à la France actuelle, et franchit le Danube atteignant ainsi le nord de l'Europe. Aux environs de 800 av. J-C, les Celtes, aussi appelés Gaulois, deviennent le principal groupe de population en Europe Occidentale. Néanmoins, ces derniers ne forment pas un peuple unique mais plutôt un ensemble de nombreuses tribus avec des coutumes similaires.

L'Europe connaîtra ensuite maints changements. D'abord l'Empire romain s'empare petit à petit du bassin méditerranéen et de l'Europe occidentale. Mais au V<sup>e</sup> siècle ap. J-C, les Romains cèdent la place à plusieurs tribus germaniques, elles-mêmes chassées de chez elles par les Huns. Avec la chute de l'Empire romain occidental commence le Moyen-Âge.

Au cours du Moyen-Âge, en Europe, les individus sédentarisés accordent une importance croissante à leurs lieux de vie, ressemblant ainsi de plus en plus à la conception d'une maison telle que nous la connaissons aujourd'hui. Ces lieux sont presque systématiquement liés à des exploitations agricoles ou des troupeaux.

Pendant le Moyen-Âge, on se déplace avec un objectif précis : le commerce, les pèlerinages ou encore la guerre. A l'exception des marchands, des pèlerins et des soldats, les individus n'avaient guère l'occasion de quitter leur terre natale. Ils étaient peu nombreux à se déplacer au cours du Moyen-Âge.

La naissance des États et des villes à partir du X<sup>e</sup> siècle renforcera encore le caractère sédentaire de ces sociétés. De plus, l'attachement à une terre natale se développe en même temps que les savoir-faire spécifiques qui s'y développent et des villes comme Bruges deviennent des carrefours culturels où se croisent des marchands issus de toute l'Europe. Avec le mouvement des hommes, on retrouve alors le mouvement des idées, des techniques et des marchandises.

{{< cartes >}}

![](/images/carte02.svg)

{{< /cartes >}}