let liste_cartes = document.getElementsByTagName("img");
let cartes = Array.from(liste_cartes);

function taille_cartes() {
    for (let img of cartes) {
        let w = img.width;
        let h = img.height;
        if (h > w) {
            img.classList.add("carte_haute");
        } else {
            img.classList.add("carte_large");;
        }
    }
}



let checkbox = document.getElementById("btn_cartes");
let div_cartes = document.getElementById("cartes");

checkbox.addEventListener('change', function() {
  if (this.checked) {
    div_cartes.style.width = "96vw";
    div_cartes.style.overflow = "auto";
  } else {
    div_cartes.style.width = "60vw";
  }
});